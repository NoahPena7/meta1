import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;


/**
 * <b>Java Drone Object</b>
 * <br><br>
 * <i>Class to send data via Sockets to Control the AR.Drone 2.0</i>
 * 
 * @author Noah Pena
 *
 */
public class JavaDrone
{
	private float yaw;
	private float pitch;
	private float gaz;
	private float roll;
	
	private boolean inAir = false;
	
	private int port = 1800;
	
	private Socket socket;
	private PrintWriter writer;
	private int weMadeIt = 3000;
	
	/**
	 * <b>Java Drone Constructor</b>
	 * <br><br>
	 * <i>Creates Drone Object</i>
	 * 
	 * @param none
	 * @param Port(optional) - Port the Client Connects To 
	 * 
	 */
	public JavaDrone()
	{
		yaw = 0;
		pitch = 0;
		gaz = 0;
		roll = 0;
		
		inAir = false;
	}
	
	/**
	 * <b>Java Drone Constructor</b>
	 * <br><br>
	 * <i>Creates Drone Object</i>
	 * 
	 * @param vPort - Port the Client connects to the C# server (1800 Default)
	 */
	public JavaDrone(int vPort)
	{
		yaw = 0;
		pitch = 0;
		gaz = 0;
		roll = 0;
		
		inAir = false;
		
		port = vPort;
	}
	
	/**
	 * <b>Drone Init</b>
	 * <br><br>
	 * <i>Initializes java client to control the AR.Drone 2.0</i>
	 * 
	 * @param none
	 * 
	 * @throws IOException
	 */
	public void init() throws IOException
	{
		inAir = false;
		
		socket = new Socket("127.0.0.1", port);
		
		writer = new PrintWriter(socket.getOutputStream());
		
        /*try {
            socket = new Socket( "127.0.0.1", 1800);
            PrintWriter writer = new PrintWriter(socket.getOutputStream());
            writer.print("Hello world");
            writer.flush();
            writer.close();
            
            socket.close();
        } catch (Exception e) {
            e.printStackTrace();
        }*/
	}
	
	/**
	 * <b>Drone Destroy</b>
	 * <br><br>
	 * <i>Call when ending drone program</i>
	 * 
	 * @param none
	 * 
	 * @throws IOException
	 */
	public void destroy() throws IOException
	{
		writer.close();
		socket.close();
	}
	
	/**
	 * <b>Update Flight Statistics</b>
	 * <br><br>
	 * <i>Adds the Pitch, Roll, Yaw, and Gaz to the current values and<br>
	 * Sends to the Drone</i>
	 * 
	 * @param vPitch - Added Value to the Pitch
	 * @param vYaw	- Added Value to the Yaw
	 * @param vRoll	- Added Value to the Roll
	 * @param vGaz	- Added Value to the Gaz
	 */
	public void updateFlightStats(float vPitch, float vYaw, float vRoll, float vGaz)
	{
		pitch += vPitch;
		yaw += vYaw;
		roll += vRoll;
		gaz += vGaz;
		
		sendData();
	}
	
	/**
	 * <b>Send Flight Statistics</b>
	 * <br><br>
	 * <i>Sends the Pitch, Yaw, Roll, and Gaz values to the AR.Drone 2.0</i>
	 * 
	 * @param vPitch - Pitch of the Drone
	 * @param vYaw	- Yaw of the Drone
	 * @param vRoll	- Roll of the Drone
	 * @param vGaz	- Gaz of the Drone
	 */
	public void sendFlightStats(float vPitch, float vYaw, float vRoll, float vGaz)
	{
		pitch = vPitch;
		yaw = vYaw;
		roll = vRoll;
		gaz = vGaz;
		
		sendData();
	}
	
	/**
	 * <b>Get Pitch</b>
	 * <br><br>
	 * <i>returns the current Pitch value</i>
	 * 
	 * @return pitch (float)
	 */
	public float getPitch()
	{
		return pitch;
	}
	
	/**
	 * <b>Get Yaw</b>
	 * <br><br>
	 * <i>returns the current Yaw value</i>
	 * 
	 * @return pitch (float)
	 */
	public float getYaw()
	{
		return yaw;
	}
	
	/**
	 * <b>Get Roll</b>
	 * <br><br>
	 * <i>returns the current Roll value</i>
	 * 
	 * @return roll (float)
	 */
	public float getRoll()
	{
		return roll;
	}
	
	/**
	 * <b>Get Gaz</b>
	 * <br><br>
	 * <i>returns the current Gaz value</i>
	 * 
	 * @return gaz (float)
	 */
	public float getGaz()
	{
		return gaz;
	}
	
	/**
	 * <b>Set Pitch</b>
	 * <br><br>
	 * <i>sets the new value to pitch</i>
	 * 
	 * @param vPitch - New Pitch of the Drone
	 */
	public void setPitch(float vPitch)
	{
		pitch = vPitch;
		
		sendData();
	}
	
	/**
	 * <b>Set Roll</b>
	 * <br><br>
	 * <i>sets the new value to roll</i>
	 * 
	 * @param vRoll - New Roll of the Drone
	 */
	public void setRoll(float vRoll)
	{
		roll = vRoll;
		
		sendData();
	}
	
	/**
	 * <b>Set Yaw</b>
	 * <br><br>
	 * <i>sets the new value to yaw</i>
	 * 
	 * @param vYaw - New Yaw of the Drone
	 */
	public void setYaw(float vYaw)
	{
		yaw = vYaw;
		
		sendData();
	}
	
	/**
	 * <b>Set Gaz</b>
	 * <br><br>
	 * <i>sets the new value to gaz</i>
	 * 
	 * @param vGaz - New Gaz of the Drone
	 */
	public void setGaz(float vGaz)
	{
		gaz = vGaz;
		
		sendData();
	}
	
	/**
	 * <b>Add Pitch</b>
	 * <br><br>
	 * <i>Adds new pitch to Current Pitch</i>
	 * 
	 * @param vPitch - New Pitch To Be Added
	 */
	public void addPitch(float vPitch)
	{
		pitch += vPitch;
		
		sendData();
	}
	
	/**
	 * <b>Add Roll</b>
	 * <br><br>
	 * <i>Adds new roll to Current Roll</i>
	 * 
	 * @param vRoll - New Roll To Be Added
	 */
	public void addRoll(float vRoll)
	{
		roll += vRoll;
		
		sendData();
	}
	
	/**
	 * <b>Add Yaw</b>
	 * <br><br>
	 * <i>Adds new yaw to Current Yaw</i>
	 * 
	 * @param vYaw - New Yaw To Be Added
	 */
	public void addYaw(float vYaw)
	{
		yaw += vYaw;
		
		sendData();
	}
	
	/**
	 * <b>Add Gaz</b>
	 * <br><br>
	 * <i>Adds new gaz to Current Gaz</i>
	 * 
	 * @param vGaz - New Gaz To Be Added
	 */
	public void addGaz(float vGaz)
	{
		gaz += vGaz;
		
		sendData();
	}
	
	/**
	 * <b>Take Off</b>
	 * <br><br>
	 * <i>Calls Drone Take Off Command</i>
	 */
	public void takeOff()
	{
		inAir = true;
		
		sendData();
	}
	
	/**
	 * <b>Land</b>
	 * <br><br>
	 * <i>Calls Drone Landing Sequence</i>
	 */
	public void land()
	{
		inAir = false;
		
		sendData();
	}
	
	/**
	 * <b>Get Debug String</b>
	 * <br><br>
	 * <i>Returns the String that is being sent to the C# Server</i>
	 * 
	 * @return String
	 */
	public String getDebugString()
	{
		return "InAir " + Boolean.toString(inAir) + " Pitch " + Float.toString(pitch) + " Roll " + Float.toString(roll) + " Yaw " + Float.toString(yaw) + " Gaz " + Float.toString(gaz);
	}
	
	private void sendData()
	{
		writer.print("InAir " + Boolean.toString(inAir) + " Pitch " + Float.toString(pitch) + " Roll " + Float.toString(roll) + " Yaw " + Float.toString(yaw) + " Gaz " + Float.toString(gaz) + "\n\n");
		//writer.print("InAir " + inAir + " Pitch " + pitch + " Roll " + roll + " Yaw " + yaw + " Gaz " + gaz + "\n\n");
		writer.flush();
	}
}
