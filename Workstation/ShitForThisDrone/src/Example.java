import java.io.IOException;



public class Example 
{
	
	public static void main(String[] args) throws IOException
	{
		float pitch, yaw, roll, gaz;
		
		//Create JavaDrone Object
		JavaDrone drone = new JavaDrone();
		
		//Initialize Drone (Needs "Throws IOException" or surround with "Try - Catch"
		drone.init();
		
		drone.takeOff();
		
		pitch = 1f;
		
		yaw = 2;
		
		roll = 0;
		gaz = 0;
		
		//Sends these flight values to the Drone
		drone.sendFlightStats(pitch, yaw, roll, gaz);
		
		//Adds -1f to the current Pitch (1f) and then sends to the Drone
		drone.addPitch(-1f);
		
		//Gets current Value of Drone Yaw and adds 1 to it and then sends to the Drone
		drone.addYaw(drone.getYaw() + 1);
		
		//Shows what the program is sending to the C# code (for debugging purposes)
		System.out.println(drone.getDebugString());

		drone.land();
		
		//Closes the Socket Transmission 
		//To use the drone again we need to call init() again
		drone.destroy();
	}

}
