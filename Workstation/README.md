**Table Of Contents**
================================================


----------------------------------------------------------------------


Blank Project
--------------------------------------------------

-This project is just a blank Unity Project with the Meta SDK already loaded in

-Delete the **"Main Camera"** Object before using this file


Hello World App
--------------------------------------------------

-This project is the "Hello World App" Example Project from the Tutorial

-The project creates an object that can be manipulated through the Meta 1 Goggles


Making a Meta Body
--------------------------------------------------

-This project is the "Making a MetaBody" Example Project from the Tutorial

-The project creates a cube object that can be manipulated by different gestures


Tracking a MarkerTarget
--------------------------------------------------

-This project is the "Tracking a MarkerTarget" Example Project from the Tutorial

-The project tracks a static 'Real-World' Marker using the Goggles Camera

