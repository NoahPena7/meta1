﻿using UnityEngine;
using System.Collections;

public class CameraSetup : MonoBehaviour 
{
    WebCamTexture webcam;

	// Use this for initialization
	void Start () 
    {
        webcam = new WebCamTexture();
        renderer.material.mainTexture = webcam;
        webcam.Play();
         
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}
}
