﻿using UnityEngine;
using System.Collections;
using Meta;

public class MyCubeScript : MetaBehaviour {

    public void OnGrab()
    {
        renderer.material.color = Color.blue;
    }

    public void OnGrabRelease()
    {
        renderer.material.color = Color.yellow;
    }

}
