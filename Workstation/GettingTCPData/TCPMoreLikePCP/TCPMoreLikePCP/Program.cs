﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.IO;


struct Drone
    {
        float pitch
        {
            get
            {
                return pitch;
            }
            set
            {
                pitch = value;
            }
        }
        float yaw;
        float roll;
        float gaz;

        bool inAir;
    };
class Program
{
    

    Drone cool = new Drone();
    

    TcpListener tcpListener;
    public void client()
    {
        try
        {
            TcpClient tc = new TcpClient("127.0.0.1", 1800);// in the place of server, enter your java server's hostname or Ip
            Console.WriteLine("Server invoked");
            NetworkStream ns = tc.GetStream();
            StreamWriter sw = new StreamWriter(ns);
            sw.WriteLine("My name is Pramod.A");
            sw.Flush();
            StreamReader sr = new StreamReader(ns);
            Console.WriteLine(sr.ReadLine());
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
        }
    }

    public void server()
    {
        IPAddress ip = Dns.GetHostEntry("localhost").AddressList[0];
        
        try
        {
            tcpListener = new TcpListener(IPAddress.Any, 1800);
            tcpListener.Start();
            tcpListener.BeginAcceptTcpClient(new AsyncCallback(this.ProcessEvents), tcpListener);

            Console.WriteLine("Listening on Port 1800");
        }
        catch (Exception e)
        {
            Console.WriteLine(e.ToString());
        }
    }

    private void ProcessEvents(IAsyncResult asyn)
    {
        try
        {
            TcpListener processListen = (TcpListener)asyn.AsyncState;
            TcpClient tcpClient = processListen.EndAcceptTcpClient(asyn);
            NetworkStream myStream = tcpClient.GetStream();
            if (myStream.CanRead)
            {
                StreamReader readerStream = new StreamReader(myStream);
               // String myMessage = readerStream.ReadLine();
                string myMessage = readerStream.ReadToEnd();
                ProcessString(myMessage);
                Console.WriteLine(myMessage);
                readerStream.Close();
                //Console.WriteLine("");
            }
            myStream.Close();
            tcpClient.Close();

            //client();

            tcpListener.BeginAcceptTcpClient(new AsyncCallback(this.ProcessEvents), tcpListener);
        }
        catch (Exception e)
        {
            Console.WriteLine(e.ToString());
        }
    }

    private void ProcessString(String output)
    {
        if (output != "")
        {
            String[] split = output.Split(' ');
            bool inAir = bool.Parse(split[1]);
            float pitch = float.Parse(split[3]);
            float roll = float.Parse(split[5]);
            float yaw = float.Parse(split[7]);
            float gaz = float.Parse(split[9]);
            bool isVertical = bool.Parse(split[11]);

            //do stuff
        }   
    }

    public static void Main(string[] args)
    {
        Program program = new Program();
        program.server();

        while (true) ;
    }

}