﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ARDrone.Control.Commands;
using ARDrone.Control;

namespace DATDRONE
{
    class Program
    {
        static void Main(string[] args)
        {
            DroneConfig config = new DroneConfig();
            config.Initialize();

            DroneControl drone = new DroneControl();
            drone.ConnectToDrone();

            PlayLedAnimationCommand animation = new PlayLedAnimationCommand(LedAnimation.Red, 25, 25);

            drone.SendCommand(animation);
                
            

        }

        
    }
}
