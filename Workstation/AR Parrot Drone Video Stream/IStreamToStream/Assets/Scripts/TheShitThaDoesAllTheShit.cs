﻿using UnityEngine;
using System.Collections;
using AR.Drone.Client;
using AR.Drone.Video;
using AR.Drone.Data;
using AR.Drone.Data.Navigation;
using FFmpeg.AutoGen;
using NativeWifi;

public class TheShitThaDoesAllTheShit : MonoBehaviour 
{

    // Stick which is moved analogical to the movement of the gamepad stick
    public Transform Stick;
    // Modifies the stick rotation
    public float StickRotationModifier = 0.15f;
    // Plane on which the main camera is mapped
    public Renderer MainRenderer;
    // Plane on which the secondary camera is mapped
    public Renderer SecondaryRenderer;
    // Rotation limit for the switch between the main camera and the secondary camera
    public float SwitchRotation = 0.4f;
    // The camera used for the switch test
    public Transform CameraForSwitchCheck;
    // Ambient Light
    public Light[] AmbientLights;
    // Status text
    public TextMesh StatusText;
    // Wifi status
    public TextMesh WifiText;
    public TextMesh WifiChart;
    public int maxChartBars = 20;

    // Gamepad variables
    private bool playerIndexSet = false;
    private XInputDotNetPure.PlayerIndex playerIndex;

    // Indicates that the drone is landed
    private bool isLanded = true;
    // Indicates that the startButton is Pressed
    private bool startButtonPressed = false;
    // Texture used for the camera content
    private Texture2D cameraTexture;
    // A black texture used for the inactive plane
    private Texture2D blackTexture;
    // byte array which will be filled by the camera data
    private byte[] data;
    // Drone variables
    private VideoPacketDecoderWorker videoPacketDecoderWorker;
    private DroneClient droneClient;
    private NavigationData navigationData;

    // Width and height if the camera
    private int width = 640;
    private int height = 360;

    // wlanclient for signal strength
    private WlanClient client;

	// Use this for initialization
	void Start () 
    {
        Debug.Log("Start This Shit");

        data = new byte[width * height * 3];

        //set Textures
        MainRenderer.material.mainTexture = cameraTexture;
        cameraTexture = new Texture2D(width, height);

        // Initialize drone
        videoPacketDecoderWorker = new VideoPacketDecoderWorker(PixelFormat.BGR24, true, OnVideoPacketDecoded);
        videoPacketDecoderWorker.Start();
        droneClient = new DroneClient("192.168.1.1");
        droneClient.UnhandledException += HandleUnhandledException;
        droneClient.VideoPacketAcquired += OnVideoPacketAcquired;
        droneClient.NavigationDataAcquired += navData => navigationData = navData;
        videoPacketDecoderWorker.UnhandledException += HandleUnhandledException;
        droneClient.Start();

        //switchDroneCamera(AR.Drone.Client.Configuration.VideoChannelType.Vertical);

        client = new WlanClient();

        Debug.Log("Oh shit Nigga");
    }
	
	// Update is called once per frame
	void Update () 
    {
        convertCameraData();

        if(Input.GetKey("escape"))
        {
            Application.Quit();
        }


	}

    private void convertCameraData()
    {
        int r = 0;
        int g = 0;
        int b = 0;
        int total = 0;
        var colorArray = new Color32[data.Length / 3];
        for (var i = 0; i < data.Length; i += 3)
        {
            colorArray[i / 3] = new Color32(data[i + 2], data[i + 1], data[i + 0], 1);
            r += data[i + 2];
            g += data[i + 1];
            b += data[i + 0];
            total++;
        }
        r /= total;
        g /= total;
        b /= total;
        cameraTexture.SetPixels32(colorArray);
        cameraTexture.Apply();
        MainRenderer.material.mainTexture = cameraTexture;
        //renderer.material.mainTexture = cameraTexture;
        Debug.Log("Nigga we Made it");
        foreach (Light light in AmbientLights)
            light.color = new Color32(System.Convert.ToByte(r), System.Convert.ToByte(g), System.Convert.ToByte(b), 1);
    }

    /// <summary>
    /// Determines what happens if a video packet is acquired.
    /// </summary>
    /// <param name="packet">Packet.</param>
    private void OnVideoPacketAcquired(VideoPacket packet)
    {
        if (videoPacketDecoderWorker.IsAlive)
            videoPacketDecoderWorker.EnqueuePacket(packet);
    }

    /// <summary>
    /// Determines what happens if a video packet is decoded.
    /// </summary>
    /// <param name="frame">Frame.</param>
    private void OnVideoPacketDecoded(VideoFrame frame)
    {
        data = frame.Data;
    }

    void HandleUnhandledException(object arg1, System.Exception arg2)
    {
        Debug.Log(arg2);
    }
}
