﻿using UnityEngine;
using System.Collections;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System;

using ISuckAtCoding;


public class Server
{
    TcpListener tcpListener;

    public void server()
    {
        IPAddress ip = Dns.GetHostEntry("localhost").AddressList[0];    

        try
        {
            tcpListener = new TcpListener(IPAddress.Any, 1800);
            tcpListener.Start();
            tcpListener.BeginAcceptTcpClient(new AsyncCallback(this.ProcessEvents), tcpListener);

            Debug.Log("Listening on Port 1800");
            //Console.WriteLine("Listening on Port 1800");
        }
        catch (Exception e)
        {
            Debug.Log(e.ToString());
            //Console.WriteLine(e.ToString());
        }
    }

    private void ProcessEvents(IAsyncResult asyn)
    {
        try
        {
            TcpListener processListen = (TcpListener)asyn.AsyncState;
            TcpClient tcpClient = processListen.EndAcceptTcpClient(asyn);
            NetworkStream myStream = tcpClient.GetStream();
            if (myStream.CanRead)
            {
                StreamReader readerStream = new StreamReader(myStream);
                // String myMessage = readerStream.ReadLine();
                string myMessage = readerStream.ReadToEnd();
                ProcessString(myMessage);
                Debug.Log(myMessage);
                //Console.WriteLine(myMessage);
                readerStream.Close();

                

                //Console.WriteLine("");
            }
            myStream.Close();
            tcpClient.Close();

            //client();

            tcpListener.BeginAcceptTcpClient(new AsyncCallback(this.ProcessEvents), tcpListener);
        }
        catch (Exception e)
        {
            Debug.Log(e.ToString());
            //Console.WriteLine(e.ToString());
        }
    }

    private void ProcessString(String output)
    {
        if (output != "")
        {
            String[] split = output.Split(' ');

            GlobalVariables.inAir = bool.Parse(split[1]);
            GlobalVariables.pitch = float.Parse(split[3]);
            GlobalVariables.roll = float.Parse(split[5]);
            GlobalVariables.yaw = float.Parse(split[7]);
            GlobalVariables.gaz = float.Parse(split[9]);
            GlobalVariables.isVertical = bool.Parse(split[11]);


            /*bool inAir = bool.Parse(split[1]);
            float pitch = float.Parse(split[3]);
            float roll = float.Parse(split[5]);
            float yaw = float.Parse(split[7]);
            float gaz = float.Parse(split[9]);
            bool isVertical = bool.Parse(split[11]);
             */

            
            //do stuff
        }
    }
	
}
