Meta 1 AR Glasses
=============================

------------------------------

*Use For Texas A&M University Corpus Christi iCORE Innovation and Research Labs*

Code in C# using Unity3D Game Engine


**Prerequisites:**
--------------------------------

- Unity 4.6 or Higher
- Meta SDK
- Windows 8/8.1


**Getting Started**
---------------------------------

1. Download SourceTree and Pull From This Repo
2. The Workstation Folder has different Unity Projects
	- The Blank Project is a Blank Project with the Meta 1 SDK already loaded in
3. To open a project in Unity open the '**unity**' file. Located in ProjectName->Assets->ProjectName.unity


**Examples**
---------------------------------

The examples for the Meta1 are located in the SDK Folder.

If you open up the **"Meta Apps"** shortcut and then go one folder back you will see the Meta SDK Folder

The HTML Meta 1 Tutorial is located in the Meta Guide Folder